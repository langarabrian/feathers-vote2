// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { data } = context;

    // Throw an error if we didn't get a vote
    if(!data.vote) {
      throw new Error('A vote must have a vote property');
    }

    // The authenticated user
    const user = context.params.user;
    // The actual vote text
    const vote = context.data.vote
      // Votes can't be longer than 400 characters
      .substring(0, 400);

    // Override the original data (so that people can't submit additional stuff)
    context.data = {
      vote,
      // Set the user id
      user: user?._id
    };

    console.log( 'vote: ' + user?.email + ' ' + vote );

    return context;
  };
};
