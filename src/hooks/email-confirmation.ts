// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, method, result, params } = context;

    const user = await app.service('users').get(result.user, params);

    console.log( 'emailed confirmation: ' + user.email );

    return context;
  };
};
