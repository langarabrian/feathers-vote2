//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'votes\' service', () => {
  let mongoServer : any;
  let app : any;
  let user : any;

  const userInfo = {
    email: 'someone@example.com',
    password: 'supersecret'
  };

  const voteInfo = {
    vote: 'Yes'
  };

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
    user = await app.service('users').create(userInfo);
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('votes');
    expect(service).toBeTruthy();
  });

  it('creates vote without user', async () => {
    await expect( async () => {
      const vote = await app.service('votes').create(voteInfo);
    }).rejects.toThrow();
  });

  it('creates vote with user', async () => {
    const vote = await app.service('votes').create( voteInfo, { user } );

    expect(vote).toBeTruthy();
  });

  it('creates duplicate vote with user', async () => {
    await expect( async () => {
      const vote = await app.service('votes').create( voteInfo, { user } );
    }).rejects.toThrow();
  });

});
